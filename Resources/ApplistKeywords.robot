*** Settings ***
Library  SeleniumLibrary
Variables  ../PageObject/Locators.py
Library  FakerLibrary  WITH NAME  faker


*** Keywords ***
# New App
Click NewApp
    Wait Until Keyword Succeeds  60  1  Click Element   ${new_app}

AppName
    Wait Until Keyword Succeeds  60  1  execute javascript  window.scrollTo(0,document.body.scrollHeight)
    Wait Until Keyword Succeeds  60  1  Click Element  ${appname}
    ${randomname}=  faker.Name
    Wait Until Keyword Succeeds  60  1  Input Text   ${appname}  ${randomname}
    Log  ${randomname}

Click Informations
    Wait Until Keyword Succeeds  30  1  Click Element  ${newapp_info_button}

Click Template
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${select_template}
    Wait Until Keyword Succeeds  30  1  Click Element  ${select_template}
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${choose_and_continue_button}
    Wait Until Keyword Succeeds  30  1  Click Element  ${choose_and_continue_button}

Click Control Panel
    Wait Until Keyword Succeeds  60  1  Click Element  ${newapp_go_to_control_panel}

Wait Until Change Template
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${change_template}

# Language Control
Click Language
    Wait Until Keyword Succeeds  30  1  Click Element    ${language_button}

Click Turkish
    Wait Until Keyword Succeeds  30  1  Click Element    ${language_turkish}

Click English
    Wait Until Keyword Succeeds  30  1  Click Element    ${language_english}

New App Text Turkish
    Wait Until Keyword Succeeds  30  1  Element Text Should Be  ${new_app}  Yeni Uygulama Oluştur

New App Text English
    Wait Until Keyword Succeeds  30  1  Element Text Should Be  ${new_app}  Create New App

# Profil Button Control
Click Profile
    Wait Until Keyword Succeeds  30  1  Click Element    ${applist_profile}

Click Profile Button
    Wait Until Keyword Succeeds  30  1  Click Element   ${profile_button}

Wait Until ProfileForm
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible   ${profile_form}

# Billing Information Control
Click Billing
    Wait Until Keyword Succeeds  60  1  Click Element    ${billing_info}

Wait Until BillingInfoForm
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible   ${billing_form}

#Payment Transaction Control
Click Payment Transaction Button
    Wait Until Keyword Succeeds  60  1  Click Element    ${payment_transaction_button}

Wait Until PaymentTransactionForm  # TST'de bu alan çalışmıyor.
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${payment_transaction_form}

#Payment Settings Control
Click Payment Settings Button
    Wait Until Keyword Succeeds  60  1  Click Element    ${payment_settings_button}

Wait Until PaymentSettingsForm
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${payment_settings_new_card}

#Notification Settings Control
Click Notification Settings Button
    Wait Until Keyword Succeeds  60  1  Click Element    ${notification_settings_button}

Wait Until NotificationSettingsForm
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${notification_settings_form}

#Logout Control
Click Logout Button
    Wait Until Keyword Succeeds  60  1  Click Element    ${logout_button}

Wait Until LoginPageForm
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${login_email}

# Edit App Control
Click App Menu Modal
    Wait Until Keyword Succeeds  60  1  Click Element    ${app_menu_modal}

Click App Edit
    Wait Until Keyword Succeeds  30  1  Click Element    ${edit_app_button}

# Notification Control
Click Notification
    Wait Until Keyword Succeeds  60  1  Click Element  ${notification_app_button}

Click Announcements
    Wait Until Keyword Succeeds  30  1  Click Element  ${announcements_button}

Title Announcements
    select window   title= Mobiroller Announcements
    Wait Until Location Is  https://news.mobiroller.com/
    Title Should Be   Mobiroller Announcements

#Search Control
Search Name
    Wait Until Keyword Succeeds  60  1  Input Text  ${search}  Chad Rodriguez

Wait Until Search App
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Not Visible   ${applist_empty_app_image}

#Push Notification Control
Click Push Notification
    Wait Until Keyword Succeeds  60  1  Click Element   ${push_notification_button}

Wait Until Push Notification Panel
    Wait Until Keyword Succeeds  60  1  Location Should Be   https://tst.my.mobiroller.com/aveNotification

#User Invite Control
Click User Invite
    Wait Until Keyword Succeeds  60  1  Click Element  ${user_invite_button}

Click User Invite Add
    Wait Until Keyword Succeeds  60  1  Click Element  ${user_invite_add_button}
    Click Element  ${user_invite_add_button}

User Invite Add Email
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${user_invite_add_email}
    ${randomemail}=  faker.Email
    Wait Until Keyword Succeeds  60  1  Input Text  ${user_invite_add_email}  ${randomemail}

Click User Invite Add Email Button
    sleep  2
    Click Element  ${user_invite_add_email_button}

Wait Success Invite Message
    Wait Until Element Is Visible  ${user_invite_success_message}

User Invite Email Verification
    Open Mailbox    host=imap.domain.com    user=email@domain.com    password=secret
    ${LATEST} =    Wait For Email    sender=noreply@domain.com    timeout=300
    ${HTML} =    Open Link From Email    ${LATEST}
    Should Contain    ${HTML}    Your email address has been updated
    Close Mailbox

#User Invite Rol Control
Select User Invite Private Rol
    Select From List By Label  ${user_invite_rol_select}  Özel

Click User Invite Rol Notification
    sleep  2
    Click Element  ${user_invite_rol_notification}

Wait Until User Invite Add
    Wait Until Element Is Visible  ${user_invite_add_button}  30 seconds

#User Invite Delete User Control
User Invite Delete User
    Wait Until Element Is Visible  ${user_invite_delete_user}  30 seconds
    sleep  1
    Click Element  ${user_invite_delete_user}

User Invite Delete Modal
    Wait Until Element Is Visible  ${user_invite_delete_modal}  30 seconds
    sleep  1
    Click Element  ${user_invite_delete_modal}

User Invite Delete Modal2
    Wait Until Element Is Visible  ${user_invite_delete_modal2}  30 seconds
    sleep  1
    Click Element  ${user_invite_delete_modal2}

#Package Upgrade Control
Click Package Upgrade
    Wait Until Keyword Succeeds  60  1  Click Element  ${package_upgrade}

Wait Until Package Modal
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${package_upgrade_modal}

#Delete App Control
First App Name
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${first_app}
    ${deleteappname1}=  Get Text  ${first_app}
    ${deleteappname2}=  set variable  ${deleteappname1}
    Set Global Variable  ${deleteappname2}

Click Delete App
    Wait Until Keyword Succeeds  30  1  Click Element  ${delete_app}

Input Delete App Name
    Wait Until Keyword Succeeds  30  1  Input Text  ${delete_app_name}  ${deleteappname2}

Click Delete Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${delete_app_button}

Check Delete App
    Wait Until Keyword Succeeds  60  1  Input Text  ${search}  ${deleteappname2}
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Not Visible  ${first_app}

#List Menu Control
Click List Menu
    Wait Until Keyword Succeeds  30  1  Click Element  ${list_menu_button}

Wait Until List Menu Dropdown
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${list_menu_bar}

#Version Notes Control
Click Version Notes
    Click Element  ${version_notes_button}

Wait Until Version Notes Page
    select window   New
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${version_notes_page_breadcrumbs}

#Privacy Policy Control
Click Privacy Policy
    Click Element  ${privacy_policy_button}

Wait Until Privacy Policy Page
    select window   New
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${privacy_policy_page_title}

#Terms of Use Control
Click Terms of Use
    Click Element  ${terms_of_use_button}

Wait Until Terms of Use Page
    select window   New
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${terms_of_use_page_title}
