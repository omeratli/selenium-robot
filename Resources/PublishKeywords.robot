*** Settings ***
Library  SeleniumLibrary
Variables  ../PageObject//Locators.py


*** Keywords ***

## Edit Market Information ##
Go Publish Page
    wait until keyword succeeds  30  1  Click Element  ${publish_page_button}

Go Edit Market
    wait until keyword succeeds  30  1  Click Element  ${publish_page_edit_market}
    ${randomtexts}=  faker.Texts
    wait until keyword succeeds  30  1  Input Text  ${publish_edit_market_description}  ${randomtexts}
    ${randomname}=  faker.Name
    wait until keyword succeeds  30  1  Input Text  ${publish_edit_market_short_description}  ${randomname}
    ${randomtext}=  faker.Text
    wait until keyword succeeds  30  1  Input Text  ${publish_edit_market_recent_changes}  ${randomtext}
    wait until keyword succeeds  30  1  Click Element  ${publish_edit_market_Terms_read}
    wait until keyword succeeds  30  1  Wait Until Element is Visible  ${publish_edit_market_Terms_read_modal}
    wait until keyword succeeds  30  1  Click Element  ${publish_edit_market_Terms_read_modal_header}
    Press Keys  None  ESC
    wait until keyword succeeds  30  1  Click Element  ${publish_edit_market_Terms}
    wait until keyword succeeds  30  1  Click Element  ${publish_edit_market_save_button}

## Market Operations ##
Check Market Operations
    wait until keyword succeeds  30  1  Click Element  ${publish_page_market_operations}
    wait until keyword succeeds  30  1  Click Element  ${publish_page_market_operations_OK_button}

## Share Your App ##
Check Share App
    wait until keyword succeeds  30  1  Click Element  ${publish_page_share_app}
    wait until keyword succeeds  30  1  Wait Until Element is Visible  ${publish_page_share_app_header}