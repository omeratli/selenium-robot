*** Settings ***
Library  SeleniumLibrary
Library  FakerLibrary  WITH NAME  faker
Variables  ../PageObject/Locators.py

*** Variables ***
${Empty}

*** Keywords ***
#Profile Page Control
Name Control
    Wait Until Keyword Succeeds  60  1  Click Element   ${profile_name}
    ${randomname}=  faker.Name
    Wait Until Keyword Succeeds  60  1  Input Text   ${profile_name}  ${randomname}
    Wait Until Keyword Succeeds  60  1  Click Element  ${profile_first_save_button}

Phone Control
    Wait Until Keyword Succeeds  60  1  Click Element   ${profile_phone}
    Wait Until Keyword Succeeds  60  1  Input Text   ${profile_phone_input}  05443332211
    Wait Until Keyword Succeeds  60  1  Click Element  ${profile_phone_save_button}
    Wait Until Keyword Succeeds  60  1  Wait Until Element is Not Visible  ${add_content_divider}
    Wait Until Keyword Succeeds  60  1  Input Text  ${profile_phone_sms_code}  12345
    Wait Until Keyword Succeeds  60  1  Click Element  ${profile_phone_sms_confirm }
    Wait Until Keyword Succeeds  120  1  Click Element  ${profile_phone_resend_sms_code}
    Wait Until Keyword Succeeds  60  1  Click Element  ${profile_phone_close_button}
    Wait Until Keyword Succeeds  60  1  Click Element  ${profile_first_save_button}

Password Same Control
    wait until keyword succeeds  60  1  Input Text  ${profile_password_old_input}  12345678
    wait until keyword succeeds  60  1  Input Text  ${profile_password_new_input}  12345678
    wait until keyword succeeds  60  1  Input Text  ${profile_password_new_confirm}  12345678
    Wait Until Keyword Succeeds  60  1  Click Element  ${profile_second_save_button}
    #Wait Until Keyword Succeeds  60  1ms  Wait Until Element is Visible  ${save_success_alert}

Password Change Control
    wait until keyword succeeds  60  1  Input Text  ${profile_password_old_input}  12345678
    wait until keyword succeeds  60  1  Input Text  ${profile_password_new_input}  123456789
    wait until keyword succeeds  60  1  Input Text  ${profile_password_new_confirm}  123456789
    Wait Until Keyword Succeeds  60  1  Click Element   ${profile_second_save_button}

Password Return Control
    wait until keyword succeeds  60  1  Input Text  ${profile_password_old_input}  123456789
    wait until keyword succeeds  60  1  Input Text  ${profile_password_new_input}  12345678
    wait until keyword succeeds  60  1  Input Text  ${profile_password_new_confirm}  12345678
    Wait Until Keyword Succeeds  60  1  Click Element   ${profile_second_save_button}

#Billing Information Page Control
Billing Info Person Control
    Wait Until Keyword Succeeds  60  1  Click Element   ${billing_info_button}
    Wait Until Keyword Succeeds  60  1  Click Element  ${billing_person}
    ${randomaddress}=  faker.Address
    Wait Until Keyword Succeeds  60  1  Input Text   ${billing_address}  ${randomaddress}
    Wait Until Keyword Succeeds  60  1  Select From List By Value  ${billing_country}  222
    Wait Until Keyword Succeeds  60  1  Select From List By Label  ${billing_city}  Antalya
    Wait Until Keyword Succeeds  60  1  Input Text   ${billing_identificationNumber}  23132132543
    Wait Until Keyword Succeeds  60  1  Click Element   ${billing_save_button}

Billing Info Company Control
    Wait Until Keyword Succeeds  60  1  Click Element  ${billing_company}
    ${randomcompany}=  faker.Company
    Wait Until Keyword Succeeds  60  1  Input Text   ${billing_company_name}  ${randomcompany}
    Wait Until Keyword Succeeds  60  1  Input Text   ${billing_tax}  Merkez
    Wait Until Keyword Succeeds  60  1  Input Text   ${billing_tax_no}  123456789
    Wait Until Keyword Succeeds  60  1  Click Element   ${billing_save_button}

#Payment Setting Page Control
Payment Setting Recurring Control
    wait until keyword succeeds  60  1  Click Element  ${payment_setting_button}
    wait until keyword succeeds  60  1  Click Element  ${payment_setting_recurring}
    wait until keyword succeeds  60  1  Wait Until Element Is Visible  ${save_success_alert}

Payment Setting New Card Control
    execute javascript  window.scrollTo(0,document.body.scrollHeight)
    Wait Until Keyword Succeeds  60  1  Click Element  ${payment_setting_new_card}
    #${randomcardnumber}=  faker.Credit Card Number
    Wait Until Keyword Succeeds  60  1  Input Text  ${payment_setting_new_card_number}  4766620000000001
    ${randomname}=  faker.Name
    Wait Until Keyword Succeeds  60  1  Input Text  ${payment_setting_new_card_name}  ${randomname}
    ${randomexpire}=  faker.Credit Card Expire
    Wait Until Keyword Succeeds  60  1  Input Text  ${payment_setting_new_card_expiry}  ${randomexpire}
    ${randomSecurityCode}=  faker.Credit Card Security Code
    Wait Until Keyword Succeeds  60  1  Input Text  ${payment_setting_new_card_cvc}  ${randomSecurityCode}
    Wait Until Keyword Succeeds  60  1  Click Element  ${payment_setting_new_card_save_button}

Payment Setting Default Card Control
    Wait Until Keyword Succeeds  60  1  Click Element  ${payment_setting_default_card}
    Wait Until Keyword Succeeds  60  1  Click Element  ${payment_setting_default_card_save_button}
    Wait Until Keyword Succeeds  60  1  Wait until Element Is Visible  ${payment_setting_default_card_badge}

Payment Setting Delete Card Control
    Wait Until Keyword Succeeds  60  1  Click Element  ${payment_setting_delete_card}
    Wait Until Keyword Succeeds  60  1  Click Element  ${payment_setting_delete_card_save_button}

#Notification Setting Page Control
Notification Setting Email Check
    Wait Until Keyword Succeeds  60  1  Click Element  ${notification_setting_button}
    Wait Until Keyword Succeeds  60  1  Click Element  ${notification_setting_email_check}
    Wait Until Keyword Succeeds  60  1  Click Element  ${notification_setting_save_button}