*** Settings ***
Library  SeleniumLibrary
Variables  ../PageObject//Locators.py
Library  FakerLibrary  WITH NAME  faker

*** Variables ***
${Empty}
${wrongicon}  ../images/app_icon.png
${icon}   ${CURDIR}/images/app_icon.png
${splash}  ${CURDIR}/images/splash_image.jpg

*** Keywords ***
#Statistic Button Control
Statistic Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${statistic_show_button}
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${statistic_date_picker}

#Push Notification Control
Push Notification
    Wait Until Keyword Succeeds  30  1  Click Element  ${push_notification_text}
    ${randomtext}=  faker.Name
    Wait Until Keyword Succeeds  30  1  Input Text  ${push_notification_text}  ${randomtext}
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${push_notification_sendFast_button}

#Language Control
Language Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${language_button_dashboard_button}

Language Turkish Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${language_turkish_dashboard_button}

Language English Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${language_english_dashboard_button}

Verify Title TR
    ${title}=    Get Title
    Wait Until Keyword Succeeds  30  1  Title Should Be  ${title}  Mobiroller Kontrol Paneli

Verify Title EN
    ${title2}=  Get Title
    Wait Until Keyword Succeeds  30  1  Title Should Be  ${title2}  Mobiroller Dashboard

#App Name Change Control
App Name Edit Button
    Wait Until Keyword Succeeds  120  1  Click Element  ${app_name_edit_button}

App Name Text
    ${randomtext}=  faker.Name
    Wait Until Keyword Succeeds  30  1  Input Text  ${app_name_text_area}  ${randomtext}

App Name Change Icon
    Choose File  ${app_name_load_icons}  ${icon}
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_name_save_icon_button}

App Name Save Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_name_save_button}

App Logo Merge
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_logo_merge_button}

App Name Text Return
    Wait Until Keyword Succeeds  30  1  Input Text  ${app_name_text_area}  Andrea Frank

Verify App Name Text
    Wait Until Keyword Succeeds  30  1  Element Text Should Be  ${app_name_text_title}  Andrea Frank

#Change Template Control
Change Template Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${change_template}

Select Template
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${select_template}
    Wait Until Keyword Succeeds  30  1  Click Element  ${select_template}
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${choose_and_continue_button}
    Wait Until Keyword Succeeds  30  1  Click Element  ${choose_and_continue_button}

Confirm Template
    Wait Until Keyword Succeeds  30  1  Click Element  ${confirm_template_button}
    Wait Until Keyword Succeeds  60  1  Click Element  ${confirm_template_go_to_control_button}

Verify Change Template Button
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${change_template}

#App Status Info Control
App Status Info Button
    Wait Until Keyword Succeeds  60  1  Click Element  ${app_status_info_button}

#App Status Splash Image Control
App Status Splash Button
    Wait Until Keyword Succeeds  60  1  Click Element  ${app_status_splash_button}

Splash Change Image
    Choose File  ${app_splash_load_image}  ${splash}
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_splash_load_image_button}
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_splash_load_image_save_button}

#App Status Content Manage Control
App Status Content Manage Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_status_content_manage_button}

Verify App Content List
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${content_module_list_page}

#App Status Add Content Control
App Status Add Content Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_status_add_content_button}

Add Standard Content
    Wait Until Keyword Succeeds  30  1  Click Element  ${add_standard_content}
    ${randomtext}=  faker.Street Name
    Wait Until Keyword Succeeds  120  5  Input Text  ${standard_content_title}  ${randomtext}
    Wait Until Keyword Succeeds  120  5  Input Text  ${standard_content_description}  ${randomtext}
    Wait Until Keyword Succeeds  30  5  Wait Until Element Is Visible  ${add_standard_content_image}
    Wait Until Keyword Succeeds  30  5  Click Element  ${content_save_button}
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Not Visible  ${save_success_alert}
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Not Visible  ${add_content_divider}
    Wait Until Keyword Succeeds  30  1  Click Element  ${dashboard_button}

#App Status Preview Control
App Status Preview Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_status_preview_button}

Verify Preview Device
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${preview_device}

#App Status Edit Profile Control
App Status Edit Profile Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_status_edit_profile_button}

App Status Edit Profile Individual Empty Control
    Wait Until Keyword Succeeds  30  1  Input Text  ${app_status_edit_profile_info_name}  ${Empty}
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_status_edit_profile_save_button}
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${app_status_edit_profile_empty_name_warning_text}

App Status Edit Profile Individual Control
    Wait Until Keyword Succeeds  30  1  Input Text  ${app_status_edit_profile_info_name}  Andrea Frank
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_status_edit_profile_save_button}

App Status Edit Profile Company Empty Control
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_status_edit_profile_info_name}
    Wait Until Keyword Succeeds  30  1  Select Radio Button  billType  company
    Wait Until Keyword Succeeds  30  1  Input Text  ${app_status_edit_profile_info_company}  ${Empty}
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_status_edit_profile_save_button}
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${app_status_edit_profile_empty_company_warning_text}

App Status Edit Profile Company Control
    Wait Until Keyword Succeeds  30  1  Input Text  ${app_status_edit_profile_info_name}  Ömer ATLI
    Wait Until Keyword Succeeds  30  1  Select Radio Button  billType  company
    ${randomcompany}=  faker.Company
    Wait Until Keyword Succeeds  30  1  Input Text  ${app_status_edit_profile_info_company}  ${randomcompany}
    Wait Until Keyword Succeeds  30  1  Click Element  ${app_status_edit_profile_save_button}

#Apk Generate Control
Apk Generate Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${apk_generate_button}

Apk Generate Next Button1
    Wait Until Keyword Succeeds  30  1  Click Element  ${apk_generate_next_button1}

Apk Generate Next Button2
    Wait Until Keyword Succeeds  30  1  Click Element  ${apk_generate_next_button2}

Apk Generate Next Button3
    Wait Until Keyword Succeeds  30  1  Click Element  ${apk_generate_next_button3}

Apk Generate Congrats Button
    Wait Until Keyword Succeeds  60  1  Click Element  ${apk_generate_congrats_button}

Verify App Generate Button Disable
    Wait Until Keyword Succeeds  30  1  Click Element  ${apk_generate_button}
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Not Visible  ${apk_generate_next_button1}