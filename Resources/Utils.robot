*** Settings ***
Library  SeleniumLibrary
Library  Collections
Resource  LoginKeywords.robot
Resource  ApplistKeywords.robot

*** Variables ***
${email3}  sewof36508@qmailers.com
${email2}    seleniumtest@test.com
${email}    testautomation@robotframework.com
${password}     ZpLyGX5EAGScSfKk
${url_tst}  https://tst.panel.mobiroller.com/
${url_stg}  https://stg.panel.mobiroller.com/
${url_tst_register}  https://tst.panel.mobiroller.com/signup
${browser}  Chrome

*** Keywords ***
start chrome options
    ${chrome_options}=  Evaluate  sys.modules['selenium.webdriver'].ChromeOptions()  sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    disable-dev-shm-usage
    Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Call Method    ${chrome_options}    add_argument    --disable-gpu
    ${ws}=    Set Variable    window-size=1920,1080
    Call Method    ${chrome options}    add_argument    ${ws}
    ${options}=  Call Method   ${chrome_options}  to_capabilities
    set global variable  ${options}

start dev session
    open browser   ${url_tst}   ${browser}
    Maximize Browser Window

start tst session
    start chrome options
    open browser   ${url_tst}   ${browser}  desired_capabilities=${options}

start stg session
    start chrome options
    open browser   ${url_stg}   ${browser}  desired_capabilities=${options}

start tst register session
    start chrome options
    open browser   ${url_tst_register}   ${browser}  desired_capabilities=${options}

start login session
    start chrome options
    open browser   ${url_tst}   ${browser}  desired_capabilities=${options}
    Enter Email     ${email}
    Enter Password  ${password}
    Click Enter Button
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${search}

start app session
    start chrome options
    open browser   ${url_tst}   ${browser}  desired_capabilities=${options}
    Enter Email     ${email}
    Enter Password  ${password}
    Click Enter Button
    #Wait Until Keyword Succeeds  60  1  Input Text  ${search}  Andrea Frank
    Wait Until Keyword Succeeds  60  1  Click Element  ${click_app_applist}
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${change_template}

start app with apk
    start chrome options
    open browser   ${url_tst}   ${browser}  desired_capabilities=${options}
    Enter Email     ${email3}
    Enter Password  ${password}
    Click Enter Button
    Wait Until Keyword Succeeds  60  1  Click Element  ${click_app_applist}
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${change_template}

start profile session
    start chrome options
    open browser   ${url_tst}   ${browser}  desired_capabilities=${options}
    Enter Email     ${email}
    Enter Password  ${password}
    Click Enter Button
    Wait Until Keyword Succeeds  60  1  Click Element  ${applist_profile}
    Wait Until Keyword Succeeds  60  5  Click Element  ${profile_button}
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${profile_form}

close session
    Close all browsers
