*** Settings ***
Library  SeleniumLibrary
Variables  ../PageObject//Locators.py

*** Keywords ***
Support Modal Control
    Wait Until Keyword Succeeds  30  1  Click Element  ${support_modal_button}
    Wait Until Keyword Succeeds  30  1  Click Element  ${support_modal_close_button}

Support Modal Design Expert
    Wait Until Keyword Succeeds  30  1  Click Element  ${support_modal_button}
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${support_modal_design_expert}

Support Modal Help Page Control
    Wait Until Keyword Succeeds  30  1  Click Element  ${support_modal_help_page_button}
    select window   New
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${support_page_search_button}



