*** Settings ***
Library  SeleniumLibrary
Library  FakerLibrary  WITH NAME  faker
Variables  ../PageObject/Locators.py

*** Variables ***
${lorem} =  Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinler olarak kullanılmıştır. Beşyüz yıl boyunca varlığını sürdürmekle kalmamış, aynı zamanda pek değişmeden elektronik dizgiye de sıçramıştır. 1960'larda Lorem Ipsum pasajları da içeren Letraset yapraklarının yayınlanması ile ve yakın zamanda Aldus PageMaker gibi Lorem Ipsum sürümleri içeren masaüstü yayıncılık yazılımları ile popüler olmuştur.


*** Keywords ***

Click Ecommerce
    Wait Until Element Is Visible  ${ecommerce_button}  60 seconds
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_button}

#ECommerce Settings Control
Ecommerce Settings
    Wait Until Element Is Visible  ${ecommerce_settings_button}  60 seconds
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_settings_button}

Ecommerce Currency Unit
    Select From List By Label  ${ecommerce_settings_currency_unit}  TRY

Ecommerce Settings Delivery
    Wait Until Element Is Visible  ${ecommerce_settings_delivery_source_code}  30 seconds
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_settings_delivery_source_code}
    Wait Until Element Is Visible  ${ecommerce_settings_delivery_source_text}  30 seconds
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_settings_delivery_source_text}
    Input Text  ${ecommerce_settings_delivery_source_text}  ${lorem}
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_settings_delivery_source_button}

Ecommerce Settings Refund
    Wait Until Element Is Visible  ${ecommerce_settings_refund_source_code}  30 seconds
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_settings_refund_source_code}
    Wait Until Element Is Visible  ${ecommerce_settings_delivery_source_text}  30 seconds
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_settings_refund_source_text}
    Input Text  ${ecommerce_settings_refund_source_text}  ${lorem}
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_settings_delivery_source_button}

Sales Contract
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_settings_sales_contract}
    Input Text  ${ecommerce_settings_sales_contract}  https://www.mobiroller.com

Notifications Emails
    Input Text  ${ecommerce_settings_notification_emails}  omer@mobiroller.com, omeratli@windowslive.com

Ecommerce Settings Payment Settings
    Click Element  ${ecommerce_settings_payment_settings}

Ecommerce Settings Payment Cash on Delivery
    Wait Until Element Is Visible  ${ecommerce_settings_payment_transfer_add_account}  30 seconds
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_settings_payment_cash_on_delivery_switch}
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_settings_payment_cash_on_delivery}
    Input Text  ${ecommerce_settings_payment_cash_on_delivery}  ${lorem}

Ecommerce Settings Transfer
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_settings_payment_transfer_switch}
    Click Element  ${ecommerce_settings_payment_transfer_switch}
    Click Element  ${ecommerce_settings_payment_transfer_add_account}
    Input Text  ${ecommerce_settings_payment_transfer_add_bank_name}  MobiBank
    Input Text  ${ecommerce_settings_payment_transfer_add_bank_account_name}  Ömer ATLI
    Input Text  ${ecommerce_settings_payment_transfer_add_account_code}  123456789
    Input Text  ${ecommerce_settings_payment_transfer_add_bank_account_number}  987654321
    Input Text  ${ecommerce_settings_payment_transfer_add_iban}  123456789123456
    Click Element  ${ecommerce_settings_payment_transfer_add_button}

Ecommerce Settings Iyzico
    Click Element  ${ecommerce_settings_iyzico_switch}
    Input Text  ${ecommerce_settings_iyzico_apiKey}  sandbox-mtkp7ghJR3lIhomlc164lHj8fzNQV5Fu
    Input Text  ${ecommerce_settings_iyzico_secretKey}  sandbox-xqKlDu6ufmwFlakcM5QynxaxMaSOSeuP

Ecommerce Settings Cargo
    Click Element  ${ecommerce_settings_cargo_settings}
    Input Text  ${ecommerce_settings_cargo_fixed_wage}  8,99
    Click Element  ${ecommerce_settings_cargo_free_switch}
    Input Text  ${ecommerce_settings_cargo_free}  50

Ecommerce Settings Save Button
    Click Element  ${ecommerce_settings_save_button}
    Wait Until Element Is Visible  ${save_success_alert}  30 seconds
    Wait Until Element Is Not Visible  ${save_success_alert}  30 seconds

#ECommerce Product Control
Ecommerce Product
    Wait Until Element Is Not Visible   xpath=//div[contains(@class,'spinner-border text-Primary')]
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_product_button}

Ecommerce Product Add Product
    Wait Until Element Is Not Visible   xpath=//div[contains(@class,'spinner-border text-Primary')]
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_product_add_button}

Ecommerce Product Add Product Name
    Wait Until Element Is Visible  ${ecommerce_product_name}
    ${randomtext}=  faker.Name
    Input Text  ${ecommerce_product_name}   ${randomtext}

Ecommerce Product Add Product Description
    Wait Until Element Is Not Visible   xpath=//div[contains(@class,'spinner-border text-Primary')]
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_product_description_source_code}
    Wait Until Element Is Not Visible   xpath=//div[contains(@class,'spinner-border text-Primary')]
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_product_description_source_text}
    ${randomtexts}=  faker.Texts
    Input Text  ${ecommerce_product_description_source_text}  ${randomtexts}
    Click Element  ${ecommerce_product_description_source_button}

Ecommerce Product Add Product Texts
    Wait Until Keyword Succeeds  30  1  Click Element  ${ecommerce_product_stock_code}
    Input Text  ${ecommerce_product_stock_code}  12345
    Input Text  ${ecommerce_product_product_price}  2222
    Input Text  ${ecommerce_product_discount_price}  1111
    #Click Element  ${ecommerce_product_shipping_switch}
    #Select From List By Label  ${ecommerce_product_shipping_price_select}  Özel
    #Input Text  ${ecommerce_product_shipping_price}  111
    Input Text  ${ecommerce_product_stock}  100
    Input Text  ${ecommerce_product_maxSales}  2
    Select From List By Label  ${ecommerce_product_status}  Aktif

Ecommerce Product Add Product Image
    Click Element  ${ecommerce_product_screenshot}
    Choose File  ${ecommerce_product_screenshot_box}  ${CURDIR}/images/6.jpg
    Click Element  ${ecommerce_product_screenshot_box_button}

    Click Element  ${ecommerce_product_screenshot}
    Choose File  ${ecommerce_product_screenshot_box}  C:/RobotGitLab/selenium-robot/images/7.jpg
    Click Element  ${ecommerce_product_screenshot_box_button}

    Click Element  ${ecommerce_product_screenshot}
    Choose File  ${ecommerce_product_screenshot_box}  C:/RobotGitLab/selenium-robot/images/8.jpg
    Click Element  ${ecommerce_product_screenshot_box_button}

Ecommerce Product List Visible
    Click Element  ${ecommerce_settings_save_button}
    Wait Until Element Is Not Visible   xpath=//div[contains(@class,'spinner-border text-Primary')]
    Wait Until Element Is Visible  ${ecommerce_product_list}  60 seconds
    Wait Until Element Is Visible  xpath=//tr[1]//td[3]  60 seconds
    #Element Text Should Be  xpath=//tr[1]//td[3]  ${lorem}
