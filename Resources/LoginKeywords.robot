*** Settings ***
Library  SeleniumLibrary
Variables  ../PageObject/Locators.py

*** Variables ***
${signupUrl}  https://tst.panel.mobiroller.com/signup

*** Keywords ***
#Login Main Page
Enter Email
    [Arguments]  ${email}
    Wait Until Keyword Succeeds  30  1  Input Text  ${login_email}  ${email}

Enter Password
    [Arguments]  ${password}
    Wait Until Keyword Succeeds  30  1  Input Text  ${login_password}  ${password}

Click Enter Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${login_button}

Press Key Enter
    Press Keys  ${login_email}  ENTER

Wrong Error Control
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${login_error_icon}

Wrong Password Error Control
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${login_error}

Invalid Error Control
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${login_invalid_error}

Wait Until Search
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${search}

language button
    Wait Until Keyword Succeeds  30  1  Click Element  ${login_language_button}

Turkish Language
    Wait Until Keyword Succeeds  30  1  Click Element  ${login_language_turkish}
    Wait Until Keyword Succeeds  30  1  Element Text Should Be  ${login_create_account_button}  Hesap oluştur

English Language
    Wait Until Keyword Succeeds  30  1  Click Element  ${login_language_english}
    Wait Until Keyword Succeeds  30  1  Element Text Should Be  ${login_create_account_button}  Create an account

#Go to Register Control
Click Create Account
    Wait Until Keyword Succeeds  30  1  Click Element  ${login_create_account_button}

Verify Signup Url
    wait until keyword succeeds  30  1  Location Should Be  ${signupUrl}