*** Settings ***
Library  SeleniumLibrary
Variables  ../PageObject/Locators.py
Library  FakerLibrary  WITH NAME  faker

*** Variables ***
${icon}   ${CURDIR}/images/app_icon.png
${splash}  ${CURDIR}/images/splash_image.jpg

*** Keywords ***
Go Manage
    wait until keyword succeeds  60  1  Click Element  ${click_manage_button}

## Form Data
Go and check Form Data
    wait until keyword succeeds  60  1  Click Element  ${click_form_data}
    wait until keyword succeeds  60  1  Wait until Element is Visible  ${form_data_gray_alert}

## Login
Go Login and Settings
    wait until keyword succeeds  60  1  Click Element  ${manage_login_button}
    wait until keyword succeeds  60  1  Click Element  ${enable_login}
    wait until keyword succeeds  60  1  Click Element  ${google_sign_in}
    wait until keyword succeeds  60  1  Click Element  ${chat_active}
    wait until keyword succeeds  60  1  Click Element  ${user_agreements_active}
    wait until keyword succeeds  60  1  Input Text  ${user_agreements_url}  123
#    wait until keyword succeeds  60  1  Click Element  xpath=//*[@href='#modal-imageupload-logoImage']
#    wait until keyword succeeds  60  1  Choose File  id=logoImage_file  ${icon}
#    wait until keyword succeeds  60  1  Click Element  id=submitImage
#    wait until keyword succeeds  60  1  Click Element  xpath=//*[@href='/aveUserLogin#modal-imageupload-loginBackgroundImage']
#    wait until keyword succeeds  60  1  Choose File  id=loginBackgroundImage_file  ${icon}
#    wait until keyword succeeds  60  1  Click Element  id=submitImage
    wait until keyword succeeds  60  1  Input Text  ${google_api_key}  123
    wait until keyword succeeds  60  1  Click Element  ${login_required_refresh_button}
    wait until keyword succeeds  60  1  Wait until Element is Not Visible  ${login_required_load_screen}
    wait until keyword succeeds  60  1  Click Element  ${manage_login_save_button}
    wait until keyword succeeds  60  1  Click Element  ${manage_login_apk_info}

Go Login and Profile Elements
#    wait until keyword succeeds  60  1  Click Element  ${manage_login_button}
    wait until keyword succeeds  60  1  Click Element  ${manage_profile_elements_button}
    wait until keyword succeeds  60  1  Click Element  ${profile_add_elements}
    wait until keyword succeeds  60  1  Select From List By Value  ${profile_select_type}  selection
    wait until keyword succeeds  60  1  Click Element  ${profile_submit_button}
    wait until keyword succeeds  60  1  Click Element  ${profile_element_edit_button}
    wait until keyword succeeds  60  1  Click Element  ${profile_submit_button}
    wait until keyword succeeds  60  1  Click Element  ${profile_element_delete_button}
    wait until keyword succeeds  60  1  Click Element  ${profile_element_save_button}

Go Login Member Managements
#    wait until keyword succeeds  60  1  Click Element  ${manage_login_button}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_managements_button}
    # New User #
    wait until keyword succeeds  60  1  Click Element  ${member_managements_add_user}
    ${randomEmail}=  faker.Email
    wait until keyword succeeds  60  1  Input Text  ${member_managements_add_user_email}  ${randomEmail}
    ${randomPassword}=  faker.Password
    wait until keyword succeeds  60  1  Input Text  ${member_managements_add_user_password}  ${randomPassword}
#    ${randomName}=  faker.Name
#    wait until keyword succeeds  60  1  Input Text  ${member_managements_add_user_nameSurname}  ${randomName}
#    ${randomText}=  faker.Text
#    wait until keyword succeeds  60  1  Input Text  ${member_managements_add_user_about}  ${randomText}
#    ${randomCity}=  faker.City
#    wait until keyword succeeds  60  1  Input Text  ${member_managements_add_user_city}  ${randomCity}
#    wait until keyword succeeds  60  1  Choose File  ${member_managements_add_user_profile_image}  ${icon}
    wait until keyword succeeds  60  1  Click Element  ${member_managements_add_user_save_button}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_managements_button}
    # Show Data #
    wait until keyword succeeds  60  1  Click Element  ${member_managements_show_data}
    wait until keyword succeeds  60  1  Click Element  ${member_managements_show_data_close_button}
    # Edit Button #
    wait until keyword succeeds  60  1  Click Element  ${member_managements_edit_button}
    wait until keyword succeeds  60  1  Click Element  ${member_managements_add_user_save_button}
    # Block user #
    wait until keyword succeeds  60  1  Click Element  ${member_managements_block_user}
    wait until keyword succeeds  60  1  Click Element  ${member_managements_block_user_save_button}
    wait until keyword succeeds  60  1  Click Element  ${member_managements_unblock_user}
    wait until keyword succeeds  60  1  Click Element  ${member_managements_block_user_save_button}
    # Remove user #
    wait until keyword succeeds  60  1  Click Element  ${member_managements_delete_user}
    wait until keyword succeeds  60  1  Click Element  ${member_managements_delete_user_save_button}

Go Login and System Role Management
#    wait until keyword succeeds  60  1  Click Element  ${manage_login_button}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_system_role_management}
    # New Role #
    wait until keyword succeeds  60  1  Click Element  ${manage_member_system_add_role}
    ${randomName}=  faker.Name
    wait until keyword succeeds  60  1  Input Text  ${manage_member_system_add_role_name}  ${randomName}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_system_add_default_role}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_system_add_save_button}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_system_role_management}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_system_edit_role}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_system_add_save_button}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_system_delete_role}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_system_delete_role_save_button}

Go Login and Chat Role Management
#    wait until keyword succeeds  60  1  Click Element  ${manage_login_button}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_chat_role_management}
    # New Chat Role #
    wait until keyword succeeds  60  1  Click Element  ${manage_member_add_chat_role}
    ${randomName}=  faker.Name
    wait until keyword succeeds  60  1  Input Text  ${manage_member_add_chat_role_name}  ${randomName}
    ${randomText}=  faker.Name
    wait until keyword succeeds  60  1  Input Text  ${manage_member_add_chat_role_description}  ${randomText}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_add_chat_role_Incognito}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_add_chat_role_badge}
    wait until keyword succeeds  60  1  Choose File  ${manage_member_add_chat_role_badge_load}  ${icon}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_add_chat_role_badge_save_button}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_add_chat_role_default}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_add_chat_role_save_button}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_add_chat_role_edit_button}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_add_chat_role_save_button}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_add_chat_role_delete_button}
    wait until keyword succeeds  60  1  Click Element  ${manage_member_add_chat_role_delete_save_button}
#    wait until keyword succeeds  60  1  Wait Until Element is Visible  ${manage_member_add_chat_role}

## Intro Message ##
Intro Message Control
    wait until keyword succeeds  60  1  Click Element  ${intro_message_button}
    wait until keyword succeeds  60  1  Click Element  ${intro_message_active_switch}
    wait until keyword succeeds  60  1  Click Element  ${intro_message_active_text}
    wait until keyword succeeds  60  1  Click Element  ${intro_message_save_button}

## Firebase Settings ##
Firebase Control
    wait until keyword succeeds  60  1  Click Element  ${firebase_settings_button}
    wait until keyword succeeds  60  1  Input Text  ${android_web_api_key}  123
    wait until keyword succeeds  60  1  Input Text  ${android_app_id}  123
    wait until keyword succeeds  60  1  Input Text  ${android_db_url}  123
    wait until keyword succeeds  60  1  Input Text  ${android_server_key}  123
    wait until keyword succeeds  60  1  Input Text  ${android_sender_id}  123
    wait until keyword succeeds  60  1  Click Element  ${firebase_settings_ios_tab}
    wait until keyword succeeds  60  1  Input Text  ${ios_web_api_key}  123
    wait until keyword succeeds  60  1  Input Text  ${ios_app_id}  123
    wait until keyword succeeds  60  1  Click Element  ${firebase_settings_save_button}







