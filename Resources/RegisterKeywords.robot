*** Settings ***
Library  SeleniumLibrary
Library  FakerLibrary  WITH NAME  faker
Variables  ../PageObject/Locators.py

*** Variables ***
${loginUrl}  https://tst.panel.mobiroller.com/login

*** Keywords ***
#Register Test
Enter Register Email
    [Arguments]  ${email}
    Wait Until Keyword Succeeds  30  1  Input Text  ${register_email}  ${email}

Enter Register Username
    [Arguments]  ${username}
    Wait Until Keyword Succeeds  30  1  Input Text  ${register_username}  ${username}

Enter Register Password
    [Arguments]  ${password}
    Wait Until Keyword Succeeds  30  1  Input Text  ${register_password}  ${password}

Click Register Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${register_button}

Wait Applist New App
    Wait Until Keyword Succeeds  30  1  Wait Until Element is Visible  ${applist_empty_app_image}

Click Register Username
    Wait Until Keyword Succeeds  30  1  Click Element  ${register_username}

#Account Close Page Control
Click Account Close
    Wait Until Keyword Succeeds  60  1  Click Element  ${applist_profile}
    Wait Until Keyword Succeeds  60  5  Click Element  ${notification_settings_button}
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${notification_settings_form}

Account Close Control
    Wait Until Keyword Succeeds  60  1  execute javascript  window.scrollTo(0,document.body.scrollHeight)
    Wait Until Keyword Succeeds  60  1  Click Element  ${close_account_button}
    Wait Until Keyword Succeeds  60  1  Click Element  ${close_account_next_button}

Account Close Enter Text Value
    ${randomatext}=  faker.Text
    Wait Until Keyword Succeeds  60  1  Input Text  ${close_account_text}  ${randomatext}

Account Close Enter Password Value
    [Arguments]  ${password}
    Wait Until Keyword Succeeds  60  1  Input Text  ${close_account_password}  ${password}

Account Close Button
    Wait Until Keyword Succeeds  60  1  Click Element  ${close_account_red_button}

# Show Password Test
Verify Closed Eye
    Wait Until Keyword Succeeds  30  1  Wait Until Element is Visible  ${register_show_closed_eye}

Click Show Password Button
    Click Element  ${register_show_password_button}

Verify Active Eye
    Wait Until Keyword Succeeds  30  1  Wait Until Element is Visible  ${register_show_active_eye}

#Terms of Use Test
Click Register Terms of Use
    Wait Until Keyword Succeeds  30  1  Click Element  ${register_terms_of_use}

#Privacy Policy Test
Click Register Privacy Policy
    Wait Until Keyword Succeeds  30  1  Click Element  ${register_privacy_policy}

#Go to Login Control
Click Login Account
    Wait Until Keyword Succeeds  30  1  Click Element  ${register_login_account_button}

Verify Login Url
    wait until keyword succeeds  30  1  Location Should Be  ${loginUrl}