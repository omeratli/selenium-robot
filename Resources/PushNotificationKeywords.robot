*** Settings ***
Library  SeleniumLibrary
Library  FakerLibrary  WITH NAME  faker
Variables  ../PageObject/Locators.py

*** Variables ***
${time}  23.07.2023

*** Keywords ***
## Send All Push Notification ##
Go Push Notification Page
    Wait Until Keyword Succeeds  60  1  Click Element  ${click_push_notification_sidebar_button}
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${push_notification_send_button}

New Push Notification
    Wait Until Keyword Succeeds  60  1  Click Element   ${push_notification_send_button}

Check Device Control
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible   ${push_notification_error_control}

Select Android Device
    Wait Until Keyword Succeeds  60  1  Click Element   ${push_notification_choose_android}

Select iOS Device
    Wait Until Keyword Succeeds  60  1  Click Element   ${push_notification_choose_ios}

Select Members
    Wait Until Keyword Succeeds  60  1  Click Element   ${push_notification_choose_users}

Enter Message Title and Description
    ${randomtext}=  faker.Text
    Wait Until Keyword Succeeds  60  1  Input Text  ${push_notification_message_title}  ${randomtext}
    Wait Until Keyword Succeeds  60  1  Input Text  ${push_notification_message}  ${randomtext}

Enter Random Image
    ${randomImageUrl}=  faker.Image Url
    Wait Until Keyword Succeeds  60  1  Input Text  ${push_notification_image}  ${randomImageUrl}

Routing With in The App
    Wait Until Keyword Succeeds  60  1  Click Element  ${push_notification_action}
    Wait Until Keyword Succeeds  60  1  Click Element  ${push_notification_action_label}
    Wait Until Keyword Succeeds  60  1  Click Element  ${push_notification_action_label_choose}

Select Web Direct
    Wait Until Keyword Succeeds  60  1  Click Element  ${push_notification_web_redirect}

Enter Random Url
    ${randomUrl}=  faker.Url
    Wait Until Keyword Succeeds  60  1  Input Text  ${push_notification_web}  ${randomUrl}

Select Web Direct in the App
    Wait Until Keyword Succeeds  60  1  Select From List By Value  ${push_notification_web_launch_url}  0

Select Web Direct in the Browser
    Wait Until Keyword Succeeds  60  1  Select From List By Value  ${push_notification_web_launch_url}  1

Select Datepicker
    Wait Until Keyword Succeeds  60  1  Click Element  ${push_notification_schedule}
    Wait Until Keyword Succeeds  60  1  Input Text  ${push_notification_send_date_picker}  ${time}

Save And Check Push Notification
    Wait Until Keyword Succeeds  60  1  Click Element  ${push_notification_save_button}
    Wait Until Keyword Succeeds  60  1  Click Element  ${push_notification_save_button2}
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${push_notification_send_button}