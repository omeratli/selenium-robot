*** Settings ***
Library  SeleniumLibrary
Variables  ../PageObject/Locators.py
Library  FakerLibrary  WITH NAME  faker

*** Variables ***
${image}   ${CURDIR}/images/1.jpg

*** Keywords ***
## Advertisement ##
Click Monetization
    Wait Until Keyword Succeeds  60  1  Click Element   ${monetization_icon_button}

Click Advertisement
    Wait Until Keyword Succeeds  60  1  Click Element   ${monetization_advertisement_button}

Android_Advertisement
    Wait Until Keyword Succeeds  60  1  Click Element   ${advertisement_android_interstitial}
    Wait Until Keyword Succeeds  60  1  Input Text   ${advertisement_android_interstitial_input}  ca-app-pub-3940256099942544/1033173712
    Wait Until Keyword Succeeds  60  1  Select From List By Value  ${advertisement_android_interstitial_LoadAuto}  60
    Wait Until Keyword Succeeds  60  1  Click Element   ${advertisement_android_banner}
    Wait Until Keyword Succeeds  60  1  Input Text   ${advertisement_android_banner_input}  ca-app-pub-3940256099942544/6300978111

iOS_Advertisement
    Wait Until Keyword Succeeds  60  1  Click Element   ${advertisement_iOS_interstitial}
    Wait Until Keyword Succeeds  60  1  Input Text   ${advertisement_iOS_interstitial_input}  ca-app-pub-3940256099942544/4411468910
    Wait Until Keyword Succeeds  60  1  Select From List By Value  ${advertisement_iOS_interstitial_LoadAuto}  30
    Wait Until Keyword Succeeds  60  1  Click Element   ${advertisement_iOS_banner}
    Wait Until Keyword Succeeds  60  1  Input Text   ${advertisement_iOS_banner_input}  ca-app-pub-3940256099942544/2934735716

Click Advertisement Save Button
    Wait Until Keyword Succeeds  60  1  Click Element   ${advertisement_save_button}
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${advertisement_android_interstitial_input}

## In-App Purchase ##
Click InAppPurchase
    Wait Until Keyword Succeeds  60  1  Click Element   ${monetization_inAppPurchase_button}

InAppPurchase Settings
    Wait Until Keyword Succeeds  60  1  Click Element   ${inAppPurchase_settings_button}
    Wait Until Keyword Succeeds  60  1  Input Text   ${inAppPurchase_settings_Android_Licence_Key_input}  123
    Wait Until Keyword Succeeds  60  1  Input Text   ${inAppPurchase_settings_iOS_Licence_Key_input}  123
    Wait Until Keyword Succeeds  60  1  Click Element   ${inAppPurchase_settings_Active}
    Wait Until Keyword Succeeds  60  1  Click Element   ${inAppPurchase_settings_save_button}

InAppPurchase New Product
    Wait Until Keyword Succeeds  60  1  Click Element   ${inAppPurchase_new_product_button}
    ${randomtext}=  faker.Text
    Wait Until Keyword Succeeds  60  1  Input Text  ${inAppPurchase_new_product_title}  ${randomtext}
    Wait Until Keyword Succeeds  60  1  Input Text  ${inAppPurchase_new_product_description}  ${randomtext}
    ${randomImageUrl}=  faker.Image Url
    Wait Until Keyword Succeeds  60  1  Input Text  ${inAppPurchase_new_product_YoutubeLink}  ${randomImageUrl}
    Wait Until Keyword Succeeds  60  1  Input Text  ${inAppPurchase_new_product_purchase}  ${randomtext}
    Wait Until Keyword Succeeds  60  1  Input Text  ${inAppPurchase_new_product_detail}  ${randomtext}
    ${randomUrl}=  faker.Url
    Wait Until Keyword Succeeds  60  1  Input Text  ${inAppPurchase_new_product_detail_page}  ${randomUrl}
    Wait Until Keyword Succeeds  60  1  Click Element  ${inAppPurchase_new_product_image_file}
    Wait Until Keyword Succeeds  60  1  Choose File  ${inAppPurchase_new_product_image}  ${image}
    Wait Until Keyword Succeeds  60  1  Click Element  ${inAppPurchase_new_product_image_save_button}
    Wait Until Keyword Succeeds  60  1  Input Text  ${inAppPurchase_new_product_android_product}  123
    Wait Until Keyword Succeeds  60  1  Input Text  ${inAppPurchase_new_product_android_onetime}  123
    Wait Until Keyword Succeeds  60  1  Input Text  ${inAppPurchase_new_product_ios_product}  456
    Wait Until Keyword Succeeds  60  1  Input Text  ${inAppPurchase_new_product_ios_onetime}  789
    Wait Until Keyword Succeeds  60  1  Click Element  ${inAppPurchase_new_product_save_button}
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Not Visible  ${inAppPurchase_new_product_save_button}
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${inAppPurchase_edit_product}

InAppPurchase Edit Product
    Wait Until Keyword Succeeds  60  1  Click Element  ${inAppPurchase_edit_product}
    Wait Until Keyword Succeeds  60  1  Click Element  ${inAppPurchase_new_product_save_button}

InAppPurchase Delete Product
    Wait Until Keyword Succeeds  60  1  Click Element  ${inAppPurchase_delete_product}
    Wait Until Keyword Succeeds  60  1  Click Element  ${inAppPurchase_delete_product_save_button}
    Wait Until Keyword Succeeds  60  1  Wait Until Element Is Visible  ${inAppPurchase_new_product_button}