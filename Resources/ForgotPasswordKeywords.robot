*** Settings ***
Library  SeleniumLibrary
Variables  ../PageObject/Locators.py

*** Keywords ***
Forgot Password Button
    Wait Until Keyword Succeeds  30  1  Click Element  ${forgot_password_button}

Enter Forgot Email
    [Arguments]  ${email}
    Wait Until Keyword Succeeds  30  1  Input Text  ${login_email}  ${email}

Send Recovery Link
    Wait Until Keyword Succeeds  30  1  Click Element  ${login_button}

Wrong Forgot Password Error Control
    Wait Until Keyword Succeeds  30  1  Wait Until Element Is Visible  ${login_error}