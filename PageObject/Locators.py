#Login Page Elements
login_email = "id=loginUserEmail"
login_password = "id=loginUserPassword"
login_button = "id=loginButton"
login_error = "xpath=//*[@class='verified err']"
login_error_icon = "class=input_inputCase__3XNU8"
login_invalid_error = "class=input_errorMessage__kmYSj"
login_language_button = "xpath=//*[@class='language-box']"
login_language_turkish = "xpath=//li[1]"
login_language_english = "xpath=//li[2]"
login_create_account_button = "xpath=//*[@href='/signup']"
forgot_password_button = "xpath=//*[@class='forgot_password']"
forgot_password_send_recovery_link_button = "id=loginButton"

#RegisterPage elements
register_username = "id=userName"
register_email = "id=loginUserEmail"
register_password = "id=userPassword"
register_button = "xpath=//*[@class='button_button__26eQg button_fullWidth__20sKC theme-button_button__yb5kR btn theme-button_bigButton__2au3f']"
register_show_password_button = "xpath=//*[@src='/static/media/blink.2d2e280e.svg']"
register_show_closed_eye = "xpath=//input[@type='password']"
register_show_active_eye = "xpath=//input[@type='text']"
register_terms_of_use = "xpath=//div[@class='authentication-footer']//a[1]"
register_privacy_policy = "xpath=//div[@class='authentication-footer']//a[2]"
register_login_account_button = "xpath=//*[@href='/login']"

#AppList elements
applist_empty_app_image = "xpath=//*[@src='/static/media/welcome.88d2b65d.svg']"
search = "id=filterInput"
click_app_applist = "xpath=//div[@class='app-card grid']"
language_button = "xpath=//*[@class='lang-dropdown_dropdownTitle__oSO0U']"
language_turkish = "xpath=//ul//div[2]//div[1]"
language_english = "xpath=//body//ul//div//div//div[2]"
applist_profile = "id=profileDropdownOpenButton"
profile_button = "id=editProfileButton"
profile_form = "id=ProfileInfoForm"
billing_info = "id=billingInfoButton"
billing_form = "id=BillingInfoForm"
payment_transaction_button = "id=editAccountPaymentTransactionsButton"
payment_transaction_form = "xpath=//*[@class='widget-main']"
payment_settings_button = "id=recurringInfoButton"
payment_settings_new_card = "xpath=//*[@class='Newcard']"
notification_settings_button = "id=notificationInfoButton"
notification_settings_form = "id=NotificationInfoForm"
logout_button = "id=logoutButton"
app_menu_modal = "xpath=//div[@class='icon appActionOpenButton']"
edit_app_button = "xpath=//a[@class='appListMenuEditButton']"
push_notification_button = "xpath=//a[@class='appListMenuNotificationButton']"
notification_app_button = "id=announceKitButton"
announcements_button = "xpath=/html/body/div/div/header/h1/a"
logout_LinkText = "Logout"
new_app = "id=headerAppCreateButton"
appname = "id=app_name"
newapp_info_button = "id=nextDesign"
#newapp_no_template = "xpath=//*[@class='btn btn-primary no-border btnNextGenerate']"
newapp_go_to_control_panel = "xpath=//*[@class='btn btn-default no-border']"

#User Invite yapılmayacak
# user_invite_button = "xpath=//li[@class='appListMenuUserInviteButton']"
# user_invite_add_button = "id=confirm"
# user_invite_add_email = "id=emailsToInvite-tokenfield"
# user_invite_add_email_button = "xpath=//button[@class='btn btn-animate btn-animate-side btn-success']"
# user_invite_success_message = "xpath=//div[@class='toast-message']"
# user_invite_rol_select = "xpath=//select[@class='form-control valid']"
# user_invite_rol_notification = "id=Notification"
# user_invite_delete_user = "xpath=//tr[1]//td[4]//button[1]"
# user_invite_delete_modal = "xpath=//button[contains(@class,'confirm btn btn-warning btn-primary')]"
# user_invite_delete_modal2 = "xpath=//button[@class='confirm btn btn-warning btn-primary']"

package_upgrade = "xpath=//*[@class='app-upgrade']"
package_upgrade_modal = "id=upgradeContent"
delete_app = "xpath=//*[@class='appListMenuDeleteButton']"
delete_app_name = "id=appName"
first_app = "xpath=//*[@class='app-card grid']"
delete_app_button = "xpath=//button[@class='btn btn-danger']"
list_menu_button = "id=listButton"
list_menu_bar = "xpath=//*[@class='app-card list']"
version_notes_button = "id=UrlVersionNotes"
version_notes_page_breadcrumbs = "id=breadcrumbs"
privacy_policy_button = "id=UrlPrivacyPolicy"
privacy_policy_page_title = "xpath=//h4[@class='fw-special-title']"
terms_of_use_button = "id=UrlTermsOfUse"
terms_of_use_page_title = "xpath=//h4[@class='fw-special-title']"

## DASHBOARD ELEMENTS ##
    #Statistic Elements
statistic_show_button = "xpath=//a[@class='btn btn-minier btn-primary']//small"
statistic_date_picker = "xpath=//b[@class='add-on']"

    #Language Elements
language_button_dashboard_button = "xpath=//body/div[@class='fixcontainer']/div[@id='wrap']/div[@id='navbar']/div[@class='navbar-inner']/div[@class='container-fluid']/ul[@class='nav ace-nav pull-right']/li[3]/a[1]"
language_turkish_dashboard_button = "xpath=//li[@class='open']//li[1]//a[1]"
language_english_dashboard_button = "xpath=//li[@class='open']//li[3]//a[1]"
dashboard_button = "xpath=//li[@id='sidebarGettingStarted']//a"

    #App Name Change Elements
app_name_edit_button = "xpath=//span[@class='badge badge-yellow edit']"
app_name_text_area = "id=title"
app_name_change_icons = "id=change-appIcons"
app_name_load_icons = "id=appIcons_file"
app_name_save_icon_button = "xpath=/html/body/div[*]/div/div[2]/div[2]/div/div/div/div[2]/div[3]/button[2]"
app_name_save_button = "id=btnSaveAppInfo"
app_logo_merge_button = "id=btnChangeImage"
app_name_text_title = "xpath=//div[@class='title']//h5"

    #Change Template Elements
change_template = "xpath=//*[@href='/Home/ChangeTemplate']"
select_template = "xpath=//*[@class='template']"
choose_and_continue_button = "xpath=//*[@class='btnNextGenerate']"
confirm_template_button = "xpath=//*[@onclick='confirmTemplate(lastPickedTemplateFolder)']"
confirm_template_go_to_control_button = "xpath=//*[@type='submit']"

    #App Status Info Elements
app_status_info_button = "xpath=/html[1]/body[1]/div[5]/div[1]/div[2]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/ul[1]/a[1]"
    #App Status Splash Image Control
app_status_splash_modal = "id=splashImage_crop-upload"
app_status_splash_button = "xpath=/html[1]/body[1]/div[5]/div[1]/div[2]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/ul[1]/a[2]"
app_splash_load_image = "id=splashImage_file"
app_splash_load_image_button = "id=submitImage"
app_splash_load_image_save_button = "id=btnSaveSplahImage"
    #App Status Content Manage Control
app_status_content_manage_button = "xpath=//div[@class='main-content']//a[3]"
content_module_list_page = "id=screenDetails"

    #App Status Add Content Control
app_status_add_content_button = "xpath=//a[@class='status_none']"

    #App Status Preview Control
app_status_preview_button = "id=doPreview"
preview_device = "id=previewFrame"

    # App Status Edit Profile Control
app_status_edit_profile_button = "xpath=//body//a[5]"
app_status_edit_profile_info_name = "id=nameSurname"
app_status_edit_profile_save_button = "xpath=//form[@id='userProfileForm']//button[@class='btn btn-small no-border btn-success']"
app_status_edit_profile_info_company = "id=companyName"
app_status_edit_profile_empty_name_warning_text = "class=help-inline"
app_status_edit_profile_empty_company_warning_text = "xpath=//div[@id='divAccountCompany']//span[@class='help-inline']"

    #APK Generator Control
apk_generate_button = "xpath=//div[@class='apk_title']"
apk_generate_next_button1 = "id=btnApkGenerate"
apk_generate_next_button2 = "id=btnApkGenerate2"
apk_generate_next_button3 = "id=btnApkGenerate4"
apk_generate_congrats_button = "xpath=//div[@id='apk_generate']//button[@class='btn btn-small no-border']"

## PROFIL ##
    #Profil Page
profile_name = "id=nameSurname"
profile_phone = "id=changePhone"
profile_phone_input = "id=pNo"
profile_phone_save_button = "id=btnSendSmsVerification"
profile_phone_sms_code = "id=smsCode"
profile_phone_sms_confirm = "id=btnConfirmCode"
profile_phone_resend_sms_code = "xpath=//a[@class='btn-link']"
profile_phone_close_button = "xpath=//button[@class='md-close close']"
profile_first_save_button = "xpath=//form[@id='ProfileInfoForm']//div//button"
profile_password_old_input = "id=oldPassword"
profile_password_new_input = "id=newPassword"
profile_password_new_confirm = "id=newPasswordConfirmation"
profile_second_save_button = "xpath=//form[@id='ChangePasswordForm']//div//button"

    #Billing Information Page
billing_info_button = "xpath=//*[@href='/aveAccountProfile/EditProfile?type=BillingInfo']"
billing_person = "xpath=//label[2]//span[1]"
billing_address = "id=address"
billing_country = "id=aveCountryID"
billing_city = "id=city"
billing_identificationNumber = "id=identificationNumber"
billing_save_button = "xpath=//*[@type='submit']"
billing_company = "xpath=//label[3]//span[1]"
billing_company_name = "id=companyName"
billing_tax = "id=taxOffice"
billing_tax_no = "id=taxNo"

    # Payment Setting Page
payment_setting_button = "xpath=//*[@href='/aveAccountProfile/EditProfile?type=RecurringInfo']"
payment_setting_recurring = "xpath=//span[@class='lbl']"
payment_setting_new_card = "xpath=//div[@class='Newcard']"
payment_setting_new_card_number = "xpath=//input[@name='number']"
payment_setting_new_card_name = "xpath=//input[@name='name']"
payment_setting_new_card_expiry = "xpath=//input[@name='expiry']"
payment_setting_new_card_cvc = "xpath=//input[@name='cvc']"
payment_setting_new_card_save_button = "xpath=//form[@id='CardForm']//div//button"
payment_setting_default_card = "xpath=//div[@id='wrap']//div[*]//a[2]"
payment_setting_default_card_save_button = "xpath=//button[@class='btn btn-primary']"
payment_setting_default_card_badge = "xpath=//span[@class='label label-success label-white middle']"
payment_setting_delete_card = "xpath=//div[3]//a[1]//span[1]"
payment_setting_delete_card_save_button = "xpath=//button[@class='btn btn-primary']"

    # Notification Setting Page
notification_setting_button = "xpath=//*[@href='/aveAccountProfile/EditProfile?type=NotificationInfo']"
notification_setting_email_check = "xpath=//*[@class='lbl']"
notification_setting_save_button = "xpath=//*[@type='submit']"

    # Close Account Page
close_account_button = "xpath=//*[@href='/aveAccountProfile/EditProfile?type=CloseAccount']"
close_account_next_button = "id=btnCloseAccountStep1"
close_account_text = "id=why"
close_account_password = "id=oldPassword"
close_account_red_button = "id=btnCloseAccountStep2"

## CONTENT ##
content_save_button = "id=sliding-btn-container-save"

    #Standard Content
add_standard_content = "xpath=//div[@id='ready']//div[1]//div[1]//div[1]//a[1]"
add_standard_content_image = "id=menuImagePreview"
standard_content_title = "xpath=//input[contains(@class,'span12')]"
standard_content_description = "xpath=//textarea[@class='autosize-transition span12']"

## GLOBAL ELEMENTS
add_content_divider = "xpath=//div[@class='modal-backdrop fade in']"
save_success_alert = "xpath=//div[@class='gritter-item']"

## MANAGE ELEMENTS ##
click_manage_button = "xpath=//*[@class='icon-gears']"
    ## Form Data ##
click_form_data = "xpath=//*[@href='/aveFormView/FormItems']"
form_data_gray_alert = "xpath=//*[@class='alert alert-grey']"

    ## Login ##
manage_login_button = "xpath=//*[@href='/aveUserLogin']"
enable_login = "id=isLoginActive"
google_sign_in = "id=isGoogleSignInActive"
chat_active = "id=isChatActive"
user_agreements_active = "id=isUserAgreementActive"
user_agreements_url = "xpath=//*[@type='text']"
google_api_key = "id=googleApiKey"
login_required_refresh_button = "id=loadScreenRefreshIcon"
login_required_load_screen = "id=loadScreenLoading"
manage_login_save_button = "xpath=//*[@class='icon-ok bigger-110']"
manage_login_apk_info = "id=submit_apk_gen_info"

    ## Login Profile Elements ##
manage_profile_elements_button = "xpath=//*[@href='#profileItems']"
profile_add_elements = "id=show-modal-addProfileItem-form"
profile_select_type = "id=type"
profile_submit_button = "id=submitprofileItem"
profile_element_edit_button = "xpath=//*[@class='btn btn-mini btn-info edit-profile-item']"
profile_element_delete_button = "xpath=//*[@class='btn btn-mini btn-danger delete-profile-item']"
profile_element_save_button = "xpath=//*[@class='btn btn btn-primary menuItem']"

    ## Login Member Managements ##
manage_member_managements_button = "xpath=//*[@href='#memberManagement']"
member_managements_add_user = "id=addUserButton"
member_managements_add_user_email = "id=pro_userName"
member_managements_add_user_password = "id=pro_password"
member_managements_add_user_nameSurname = "xpath=/html[1]/body[1]/div[5]/div[1]/div[2]/div[3]/div[2]/div[1]/div[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[3]/div[1]/input[1]"
member_managements_add_user_about = "xpath=//*[@data-ismandatory='False']"
member_managements_add_user_city = "id=id_5fa53a9d3b6a643c713da898"
member_managements_add_user_profile_image = "xpath=//*[@class='profilePhoto']"
member_managements_add_user_save_button = "id=submitUser"
member_managements_show_data = "xpath=//*[@class='btn btn-mini btn-success showData']"
member_managements_show_data_close_button = "xpath=//*[@class='btn btn-small no-border']"
member_managements_edit_button = "id=editUser"
member_managements_block_user = "xpath=//*[@class='btn btn-mini btn-danger blockUser tooltip-error']"
member_managements_block_user_save_button = "xpath=//*[@data-bb-handler='confirm']"
member_managements_unblock_user = "xpath=//*[@class='btn btn-mini btn-warning unblockUser tooltip-warning']"
member_managements_delete_user = "xpath=//*[@class='btn btn-mini btn-danger deleteUser']"
member_managements_delete_user_save_button = "id=deleteUser"

    ## Login System Role Managements ##
manage_member_system_role_management = "xpath=//*[@href='#roleManagement']"
manage_member_system_add_role = "id=addRoleButton"
manage_member_system_add_role_name = "id=roleName"
manage_member_system_add_default_role = "id=isDefaultRole"
manage_member_system_add_save_button = "id=submitRole"
manage_member_system_edit_role = "id=editRole"
manage_member_system_delete_role = "xpath=//*[@href='#modal-deleteRole-form']"
manage_member_system_delete_role_save_button = "xpath=//*[@data-bb-handler='confirm']"


    ## Login Chat Role Managements ##
manage_member_chat_role_management = "xpath=//*[@href='#chatRoleManagement']"
manage_member_add_chat_role = "id=addChatRoleButton"
manage_member_add_chat_role_name = "id=aveChatRoleName"
manage_member_add_chat_role_description = "id=aveChatRoleDescription"
manage_member_add_chat_role_Incognito = "id=isIncognito"
manage_member_add_chat_role_badge = "id=addRibbonModal"
manage_member_add_chat_role_badge_load = "id=ribbonImage_file"
manage_member_add_chat_role_badge_save_button = "id=submitImage"
manage_member_add_chat_role_default = "id=isDefaultChatRole"
manage_member_add_chat_role_save_button = "id=submitChatRole"
manage_member_add_chat_role_edit_button = "id=editChatRole"
manage_member_add_chat_role_delete_button = "xpath=//*[@class='btn btn-mini btn-danger deleteChatRole']"
manage_member_add_chat_role_delete_save_button = "xpath=//*[@data-bb-handler='confirm']"

    ## Intro Message ##
intro_message_button = "xpath=//*[@href='/aveIntroMessage/AddOrEdit']"
intro_message_active_switch = "id=isActiveChk"
intro_message_active_text = "xpath=//*[@type='text']"
intro_message_save_button = "xpath=//*[@class='icon-ok bigger-110']"

    ## Firebase Settings ##
firebase_settings_button = "xpath=//*[@href='/FirebaseSetting/AddOrEdit']"
android_web_api_key = "id=AndroidWebApiKey"
android_app_id = "id=AndroidAppId"
android_db_url = "id=AndroidDbURL"
android_server_key = "id=AndroidServerKey"
android_sender_id = "id=AndroidSenderId"
firebase_settings_ios_tab = "xpath=//*[@href='#ios']"
ios_web_api_key = "id=IosWebApiKey"
ios_app_id = "id=IosAppId"
firebase_settings_save_button = "xpath=//*[@class='icon-ok bigger-110']"

## MONETİZATİON ELEMENTS ##
monetization_icon_button = "class=icon-money"

    ## Advertisement ##
monetization_advertisement_button = 'xpath=//*[@id="sidebar"]/ul/li[6]/ul/li[1]'
advertisement_android_interstitial = "id=isinterstitialActive"
advertisement_android_interstitial_input = "id=AndroidInterstitial"
advertisement_android_interstitial_LoadAuto = "id=autoIntFreq"
advertisement_android_banner = "isbannerActive"
advertisement_android_banner_input = "id=AndroidBanner"
advertisement_iOS_interstitial = "id=isiosinterstitialActive"
advertisement_iOS_interstitial_input = "id=IOSInterstitial"
advertisement_iOS_interstitial_LoadAuto = "id=autoIntFreqIOS"
advertisement_iOS_banner = "id=isiosbannerActive"
advertisement_iOS_banner_input = "id=IOSBanner"
advertisement_save_button = "xpath=//*[@id='aveAdCode']/div[2]/a[1]"

    ## In-App Purchase ##
monetization_inAppPurchase_button = 'xpath=//*[@id="sidebar"]/ul/li[6]/ul/li[2]/a'
inAppPurchase_settings_button = 'xpath=//*[@href="/aveInAppPurchase/Settings"]'
inAppPurchase_settings_Android_Licence_Key_input = "id=androidLicenceKey"
inAppPurchase_settings_iOS_Licence_Key_input = "id=iosSharedSecretKey"
inAppPurchase_settings_Active = "id=isActive"
inAppPurchase_settings_save_button = "xpath=//*[@form='aveInAppPurchaseForm']"
inAppPurchase_new_product_button = "xpath=//*[@href='/aveInAppPurchase/prtAddOrEdit']"

    ## In-App Purchase New Product ##
inAppPurchase_new_product_title = "xpath=//*[@class='span6']"
inAppPurchase_new_product_description = "xpath=//*[@class='limited span6']"
inAppPurchase_new_product_YoutubeLink = "xpath=//*[@class='span6 limited']"
inAppPurchase_new_product_purchase = "xpath=//*[@name='buyActionText_EN']"
inAppPurchase_new_product_detail = "xpath=//*[@name='detailActionText_EN']"
inAppPurchase_new_product_detail_page = 'xpath=//*[@placeholder="https://www.website.com"]'
inAppPurchase_new_product_image_file = "id=gwt-uid-507"
inAppPurchase_new_product_image = "id=productImage_file"
inAppPurchase_new_product_image_save_button = "id=submitImage"
inAppPurchase_new_product_android_product = "id=androidProductIDs"
inAppPurchase_new_product_android_onetime = "id=androidOneTimeID"
inAppPurchase_new_product_ios_product = "id=iosProductIDs"
inAppPurchase_new_product_ios_onetime = "id=iosOneTimeID"
inAppPurchase_new_product_save_button = "xpath=//*[@onclick='submitIapForm()']"

    ## In-App Purchase Edit Product ##
inAppPurchase_edit_product = "xpath=//*[@class='green']"

    ## In-App Purchase Delete Product ##
inAppPurchase_delete_product = "xpath=//*[@class='red delete-product']"
inAppPurchase_delete_product_save_button = "xpath=//*[@data-bb-handler='confirm']"

    ## ECOMMERCE ELEMENTS ##
ecommerce_button = "xpath=//div[@id='sidebar']//li[7]"

    #Ecommerce Settings Elements
ecommerce_settings_button = "xpath=//li[@class='open']//li[3]"
ecommerce_settings_currency_unit = "id=Definitions_DefaultCurrency"
ecommerce_settings_delivery_source_code = "id=mceu_22-button"
ecommerce_settings_delivery_source_text = "xpath=//textarea[@class='mce-textbox mce-multiline mce-abs-layout-item mce-first mce-last']"
ecommerce_settings_delivery_source_button = "xpath=//div[@class='mce-widget mce-btn mce-primary mce-abs-layout-item mce-first mce-btn-has-text']//button"
ecommerce_settings_refund_source_code = "id=mceu_73-button"
ecommerce_settings_refund_source_text = "xpath=//textarea[@class='mce-textbox mce-multiline mce-abs-layout-item mce-first mce-last']"
ecommerce_settings_refund_source_button = "xpath=//div[@class='mce-widget mce-btn mce-primary mce-abs-layout-item mce-first mce-btn-has-text']//button"
ecommerce_settings_sales_contract = "id=Definitions_DistanceSalesContractURL_TR_"
ecommerce_settings_notification_emails = "id=Definitions_NotifyEmails"
ecommerce_settings_save_button = "xpath=//div[@class='main-content']//a[3]"
ecommerce_settings_payment_settings = "xpath=//div[@class='main-content']//li[2]//a[1]"
ecommerce_settings_payment_cash_on_delivery_switch = "id=PaymentSettings_IsPayAtDoorActive"
ecommerce_settings_payment_cash_on_delivery = "id=form-field-8"
ecommerce_settings_payment_transfer_switch = "id=PaymentSettings_IsTransferActive"
ecommerce_settings_payment_transfer_add_account = "id=paymentAccountModalButton"
ecommerce_settings_payment_transfer_add_bank_name = "name=modal_PaymentSettings.PaymentAccounts[-1].Name"
ecommerce_settings_payment_transfer_add_bank_account_name = "name=modal_PaymentSettings.PaymentAccounts[-1].AccountName"
ecommerce_settings_payment_transfer_add_account_code = "name=modal_PaymentSettings.PaymentAccounts[-1].AccountCode"
ecommerce_settings_payment_transfer_add_bank_account_number = "name=modal_PaymentSettings.PaymentAccounts[-1].AccountNumber"
ecommerce_settings_payment_transfer_add_iban = "name=modal_PaymentSettings.PaymentAccounts[-1].AccountAddress"
ecommerce_settings_payment_transfer_add_button = "id=paymentAccountSubmit"
ecommerce_settings_iyzico_switch = "name=PaymentSettings.PaymentProviders[0].IsActive"
ecommerce_settings_iyzico_apiKey = "name=PaymentSettings.PaymentProviders[0].Configuration[apiKey]"
ecommerce_settings_iyzico_secretKey = "name=PaymentSettings.PaymentProviders[0].Configuration[secretKey]"
ecommerce_settings_cargo_settings = "xpath=//div[@class='main-content']//li[3]//a[1]"
ecommerce_settings_cargo_fixed_wage = "id=ShippingSettings_FixPrice"
ecommerce_settings_cargo_free_switch = "id=ShippingSettings_IsShippingPriceThresholdActive"
ecommerce_settings_cargo_free = "id=ShippingSettings_FreeShippingPriceThreshold"

    #Ecommerce Product Elements
ecommerce_product_button = "xpath=//li[@class='open']//li[1]"
ecommerce_product_add_button = "xpath=//a[@class='btn btn-primary no-border pull-right']"
ecommerce_product_name = "id=Product_Title_TR_"
ecommerce_product_description_source_code = "xpath=//i[@class='mce-ico mce-i-code']"
ecommerce_product_description_source_text = "xpath=//textarea[@class='mce-textbox mce-multiline mce-abs-layout-item mce-first mce-last']"
ecommerce_product_description_source_button = "xpath=//div[@class='mce-widget mce-btn mce-primary mce-abs-layout-item mce-first mce-btn-has-text']//button"
ecommerce_product_stock_code = "id=Product_StockCode"
ecommerce_product_product_price = "id=Product_Price"
ecommerce_product_discount_price = "id=Product_DiscountedPrice"
ecommerce_product_shipping_switch = "id=Product_IsFreeShipping"
ecommerce_product_shipping_price_select = "id=Product_Shipping"
ecommerce_product_shipping_price = "id=Product_ShippingPrice"
ecommerce_product_stock = "id=Product_Stock"
ecommerce_product_maxSales = "id=Product_MaxSales"
ecommerce_product_status = "id=Product_Status"
ecommerce_product_screenshot = "id=gwt-uid-507"
ecommerce_product_screenshot_box = "id=photoUpload"
ecommerce_product_screenshot_box_button = "xpath=//a[@class='btn btn-small btn-primary']"
ecommerce_product_save_button = "xpath=//div[@class='main-content']//a[3]"
ecommerce_product_list = "id=productsList"

## PUBLISH PAGE ##
publish_page_button = "id=sidebarPublishment"

    ## Edit Market Informataion ##
publish_page_edit_market = "xpath=//*[@href='/avePublishment/AddOrEdit?isNew=1']"
publish_edit_market_description = "id=description"
publish_edit_market_short_description = "id=shortDescription"
publish_edit_market_recent_changes = "id=recentChanges"
publish_edit_market_keywords = "xpath=//*[@class='tags']"
publish_edit_market_category = "id=category"
publish_edit_market_contentRating = "id=contentRating"
publish_edit_market_Terms = "id=chkTerms"
publish_edit_market_Terms_read = "xpath=//*[@href='#modal-announce']"
publish_edit_market_Terms_read_modal = "id=modal-announce"
publish_edit_market_Terms_read_modal_header = "class=table-header"
publish_edit_market_save_button = "id=savePublishment"

    ## Market Operations ##
publish_page_market_operations = "xpath=//*[@class='btn btn-large no-border btn-purple']"
publish_page_market_operations_OK_button = "id=btnMobirollerPublishmentRequest"

    ## Share Your App ##
publish_page_share_app = "xpath=//*[@class='btn btn-large no-border btn-yellow']"
publish_page_share_app_header = "xpath=//*[@class='page-header position-relative']"

## PUSH NOTIFACTION ELEMENTS ##
push_notification_text = "id=notificationMessage"
push_notification_sendFast_button = "id=sendFastNotification"
click_push_notification_sidebar_button = 'xpath=//*[@href="/aveNotification"]'
push_notification_send_button = "xpath=//*[@class='btn btn-small btn-success shortcut-add-new tooltip-success']"
push_notification_error_control = "xpath=//*[@class='alert alert-error']"
push_notification_choose_android = "id=selectFilter.android"
push_notification_choose_ios = "id=selectFilter.ios"
push_notification_choose_users = "id=selectFilter.users"
push_notification_message_title = "id=notificationTitle_EN"
push_notification_message = "id=notificationMessage_EN"
push_notification_image = "id=notificationImage_EN"
push_notification_action = "id=ActionType.Screen"
push_notification_action_label = "xpath=//*[@class='btn dropdown-toggle']"
push_notification_action_label_choose = "id=aveAccountScreenID_3239"
push_notification_web_redirect = "id=ActionType.Redirect"
push_notification_web = "id=launchURL_EN"
push_notification_web_launch_url = "id=LaunchUrlType"
push_notification_schedule = "id=NotificationScheduleType.Planned"
push_notification_send_date_picker = "id=SendDateDatepicker"
push_notification_save_button = "xpath=//*[@class='icon-ok bigger-110']"
push_notification_save_button2 = "xpath=//*[@data-bb-handler='confirm']"


## SUPPORT MODAL ##
support_modal_button = "class=icon-question"
support_modal_help_page_button = "xpath=//*[@href='http://support.mobiroller.com/']"
support_page_search_button = "id=s"
support_modal_design_expert = "xpath=//*[text()='support@mobiroller.com']"
support_modal_close_button = 'xpath=//*[@id="m_support"]/div/div[4]/button'
