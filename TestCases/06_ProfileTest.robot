*** Settings ***
Resource   ../Resources/Utils.robot
Resource   ../Resources/ProfileKeywords.robot
Library  SeleniumLibrary
Library  FakerLibrary  WITH NAME  faker
Test Setup  Utils.start profile session
Test Teardown  Utils.close session

*** Test Cases ***
Profile Page Control
    Name Control
    #Phone Control
    Password Same Control
    Password Change Control
    Password Return Control

Billing Information Control
    Billing Info Person Control
    Billing Info Company Control

Payment Setting Control
    Payment Setting Recurring Control
    Payment Setting New Card Control
    Payment Setting Default Card Control
    Payment Setting Delete Card Control

Notification Setting Control
    Notification Setting Email Check

#Close Account Control
  # Bu test register alanına taşınmıştır.