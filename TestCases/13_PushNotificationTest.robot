*** Settings ***
Resource  ../Resources/Utils.robot
Resource  ../Resources/PushNotificationKeywords.robot
Library  SeleniumLibrary
Test Setup  Utils.start app with apk
Test Teardown  Utils.close session

*** Test Cases ***
Send All Push Notification  ## Burada hem tüm cihazlara göndermeyi hem de "Web Adresi Aç - Uygulamanın içinde" özelliğini test ediyoruz. ##
    Go Push Notification Page
    New Push Notification
    Check Device Control
    Enter Message Title and Description
    Enter Random Image
    Select Web Direct
    Enter Random Url
    Select Web Direct in the App
    Save And Check Push Notification

Send Android Push Notification  ## Burada hem Android cihazlara göndermeyi hem de Yönlendirilecek Ekran özelliğini test ediyoruz. ##
    Go Push Notification Page
    New Push Notification
    Check Device Control
    Select Android Device
    Enter Message Title and Description
    Enter Random Image
    Routing With in The App
    Save And Check Push Notification

Send iOS Push Notification  ## Burada hem ios cihazlara göndermeyi hem de planlı gönder özelliğini test ediyoruz. ##
    Go Push Notification Page
    New Push Notification
    Check Device Control
    Select iOS Device
    Enter Message Title and Description
    Enter Random Image
    Select Datepicker
    Save And Check Push Notification

Send Users Push Notification  ## Burada hem kullanıcılara özel göndermeyi hem de "Web Adresi Aç - Tarayıcıda" özelliğini test ediyoruz. ##
    Go Push Notification Page
    New Push Notification
    Check Device Control
    Select Members
    Enter Message Title and Description
    Enter Random Image
    Select Web Direct
    Enter Random Url
    Select Web Direct in the Browser
    Save And Check Push Notification