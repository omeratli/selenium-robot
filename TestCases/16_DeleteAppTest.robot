*** Settings ***
Resource   ../Resources/Utils.robot
Resource  ../Resources/ApplistKeywords.robot
Library  SeleniumLibrary
Test Setup  Utils.start login session
Test Teardown  Utils.close session

*** Test Cases ***
Delete App Control
    First App Name
    Click App Menu Modal
    Click Delete App
    Input Delete App Name
    Click Delete Button
    Check Delete App