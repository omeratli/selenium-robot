*** Settings ***
Resource   ../Resources/Utils.robot
Resource   ../Resources/LoginKeywords.robot
Resource   ../Resources/ForgotPasswordKeywords.robot
Library  SeleniumLibrary
Test Setup  Utils.start tst session
Test Teardown  Utils.close session

*** Variables ***
${email}    testautomation@robotframework.com
${password}     ZpLyGX5EAGScSfKk
${wrongemail}   omer1111@mobiroller
${wrongemail2}   omer1111@mobiroller.com
${wrongpassword}    12345678999
${Empty}

*** Test Cases ***
Forgot Password
    Forgot Password Button
    Enter Forgot Email  ${email}
    Send Recovery Link

Wrong Email Forgot Password
    Forgot Password Button
    Enter Forgot Email  ${wrongemail2}
    Send Recovery Link
    Wrong Forgot Password Error Control

Missing Email Forgot Password
    Forgot Password Button
    Enter Forgot Email  ${wrongemail}
    Send Recovery Link
    Invalid Error Control