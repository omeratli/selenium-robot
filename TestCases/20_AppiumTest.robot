*** Settings ***
Resource  ../Resources/AppiumTestKeywords.robot

*** Test Cases ***
Should send keys to search box and then check the value
  Open Test Application
  Input Search Query  Hello World!
  Submit Search
  Search Query Should Be Matching  Hello World!