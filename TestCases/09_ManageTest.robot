*** Settings ***
Resource  ../Resources/Utils.robot
Resource  ../Resources/ManageKeywords.robot
Library  SeleniumLibrary
Library  FakerLibrary  WITH NAME  faker
Test Setup  Utils.start app session
Test Teardown  Utils.close session


*** Test Cases ***
Form Data Control
    Go Manage
    Go and check Form Data

Login Control
    Go Manage
    Go Login and Settings
    Go Login and Profile Elements
    Go Login Member Managements
    Go Login and System Role Management
    Go Login and Chat Role Management

Intro Message Control
    Go Manage
    Intro Message Control
    
Firebase Control
    Go Manage
    Firebase Control