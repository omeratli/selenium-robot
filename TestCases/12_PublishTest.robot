*** Settings ***
Resource   ../Resources/Utils.robot
Resource  ../Resources/PublishKeywords.robot
Library  SeleniumLibrary
Test Setup  Utils.start app with apk
Test Teardown  Utils.close session

*** Test Cases ***
Edit Market Information  ## Burada seçtiğimiz bir hesaptaki APK'sı olan bir uygulamayı baz aldık ve testleri buna göre düzenledik.
    Go Publish Page
    Go Edit Market

Market Operations  ## Burada market bilgilerini gönderdikten sonra, "yayınlama talebi değerlendirilmektedir" modalını kontrol ediyoruz.
    Go Publish Page
    Check Market Operations

Share Your App  ## Burada "Tanıtım > Araçlar" sayfasına göndermesini bekliyoruz.
    Go Publish Page
    Check Share App