*** Settings ***
Resource  ../Resources/Utils.robot
Resource  ../Resources/LoginKeywords.robot
Library  SeleniumLibrary
Test Setup  Utils.start tst session
Test Teardown  Utils.close session

*** Variables ***
${email}    testautomation@robotframework.com
${password}     ZpLyGX5EAGScSfKk
${invalidemail}   omer1111@mobiroller
${wrongpassword}    12345678999


*** Test Cases ***
Login Test
    Enter Email     ${email}
    Enter Password  ${password}
    Click Enter Button
    Wait Until Search

Wrong Email
    Enter Email     ${invalidemail}
    Enter Password  ${password}
    Wrong Error Control

Wrong Password
    Enter Email     ${email}
    Enter Password  ${wrongpassword}
    Click Enter Button
    Wrong Password Error Control

Language Test
    Language Button
    Turkish Language
    Language Button
    English Language

Go to Register Control
    Click Create Account
    Verify Signup Url