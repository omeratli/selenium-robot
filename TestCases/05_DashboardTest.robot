*** Settings ***
Resource   ../Resources/Utils.robot
Resource  ../Resources/DashboardKeywords.robot
Library  SeleniumLibrary
Library  FakerLibrary  WITH NAME  faker
Test Setup  Utils.start app session
Test Teardown  Utils.close session

*** Test Cases ***
Statistic Button Control
    Statistic Button

Push Notification Control
    Push Notification

Language Control
    DashboardKeywords.Language Button
    Language Turkish Button
    Verify Title TR
    DashboardKeywords.Language Button
    Language English Button
    Verify Title EN

App Name Control
    App Name Edit Button
    App Name Text
    App Name Change Icon
    App Name Save Button
    App Logo Merge

App Status Info Control
    App Status Info Button
    App Name Text
    App Name Change Icon
    App Name Save Button
    App Logo Merge

App Status Splash Control
    App Status Splash Button
    Splash Change Image
    Verify Change Template Button

App Status Content Manage Control
    App Status Content Manage Button
    Add Standard Content

App Status Preview Control
    App Status Preview Button
    Verify Preview Device

App Status Edit Profile Control
    App Status Edit Profile Button
    App Status Edit Profile Individual Empty Control
    App Status Edit Profile Individual Control
    App Status Edit Profile Button
    App Status Edit Profile Company Empty Control
    App Status Edit Profile Company Control

Apk Generate Control
    Apk Generate Button
    Apk Generate Next Button1
    Apk Generate Next Button2
    Apk Generate Congrats Button
    Verify App Generate Button Disable

Change Template Control
    Change Template Button
    Select Template
    Confirm Template
    Verify Change Template Button