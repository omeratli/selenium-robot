*** Settings ***
Resource  ../Resources/Utils.robot
Resource  ../Resources/ApplistKeywords.robot
Library  SeleniumLibrary
Library  FakerLibrary  WITH NAME  faker
Test Setup  Utils.start login session
Test Teardown  Utils.close session


*** Test Cases ***
Search Control
    Search Name
    Wait Until Search App

NewApp
    Click NewApp
    AppName
    Click Informations
    Click Template
    Click Control Panel
    Wait Until Change Template

Language Control
    Click Language
    Click Turkish
    New App Text Turkish
    Click Language
    Click English
    New App Text English

Notification Control
    Click Notification

Profil Button Control
    Click Profile
    Click Profile Button
    Wait Until ProfileForm

Billing Information Control
    Click Profile
    Click Billing
    Wait Until BillingInfoForm

Payment Transaction Control
    Click Profile
    Click Payment Transaction Button
    #Wait Until PaymentTransactionForm  Test ortamında burası çalışmıyor.

Payment Settings Control
    Click Profile
    Click Payment Settings Button
    Wait Until PaymentSettingsForm

Notification Settings Control
    Click Profile
    Click Notification Settings Button
    Wait Until NotificationSettingsForm

Logout Control
    Click Profile
    Click Logout Button
    Wait Until LoginPageForm

Edit App Control
    Click App Menu Modal
    Click App Edit
    Wait Until Change Template

Push Notification Control
    Click App Menu Modal
    Click Push Notification
    Wait Until Push Notification Panel

#User Invite Control  #Burası yapılmadı
#    Search Name
#    Click App Menu Modal
#    Click User Invite
#    Click User Invite Add
#    User Invite Add Email
#    Click User Invite Add Email Button
#    Wait Until User Invite Add
#    Wait Success Invite Message

#User Invite Rol Control  #Burası yapılmadı
#    Click Dropdown
#    Click User Invite Dropdown
#    Click User Invite Add
#    User Invite Add Email
#    Select User Invite Private Rol
#    Click User Invite Rol Notification
#    Click User Invite Add Email Button
#    Wait Until User Invite Add
#    Wait Success Invite Message

#User Invite Delete Control  #Burası yapılmadı
#    Click Dropdown
#    Click User Invite Dropdown
#    User Invite Delete User
#    User Invite Delete Modal
#    User Invite Delete Modal2

Package Upgrade Control
    Click App Menu Modal
    Click Package Upgrade
    Wait Until Package Modal

List Menu Control
    Click List Menu
    Wait Until List Menu Dropdown

Version Notes Control
    Click Version Notes
    Wait Until Version Notes Page

Privacy Policy Control
    Click Privacy Policy
    Wait Until Privacy Policy Page

Terms of Use Control
    Click Terms of Use
    Wait Until Terms of Use Page