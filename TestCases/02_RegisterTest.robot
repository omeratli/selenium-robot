*** Settings ***
Resource   ../Resources/Utils.robot
Resource   ../Resources/RegisterKeywords.robot
Resource   ../Resources/LoginKeywords.robot
Resource   ../Resources/ApplistKeywords.robot
Library  SeleniumLibrary
Library  FakerLibrary  WITH NAME  faker
Test Setup  Utils.start tst register session
Test Teardown  Utils.close session

*** Variables ***
${invalidemail}   omer1111@mobiroller

*** Test Cases ***
Register Test
    ${email}=  faker.Email
    Enter Register Email  ${email}
    ${username}=  faker.Name
    Enter Register Username  ${username}
    ${pass}=  faker.Password
    Enter Register Password  ${pass}
## Show Password Test ##
    Verify Closed Eye
    Click Show Password Button
    Verify Active Eye
    Click Register Button
    Enter Password  ${pass}
    Enter Email   ${email}
    Click Enter Button
    Wait Applist New App
## Close Account Control ##
    Click Account Close
    Account Close Control
    Account Close Enter Text Value
    Account Close Enter Password Value  ${pass}
    Account Close Button

Invalid Email Test
    Enter Register Email  ${invalidemail}
    Click Register Username
    Invalid Error Control

Show Password Test
    ${email}=  faker.Email
    Enter Register Email  ${email}
    ${username}=  faker.Name
    Enter Register Username  ${username}
    ${pass}=  faker.Password
    Enter Register Password  ${pass}
    Verify Closed Eye
    Click Show Password Button
    Verify Active Eye

Terms of Use Test
    Click Register Terms of Use
    Wait Until Terms of Use Page

Privacy Policy Test
    Click Register Privacy Policy
    Wait Until Privacy Policy Page

Go to Login Control
    Click Login Account
    Verify Login Url
