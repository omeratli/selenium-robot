*** Settings ***
Resource   ../Resources/Utils.robot
Resource  ../Resources/MonetizationKeywords.robot
Library  SeleniumLibrary
Library  FakerLibrary  WITH NAME  faker
Test Setup  Utils.start app session
Test Teardown  Utils.close session

*** Test Cases ***
Advertisement Control
    Click Monetization
    Click Advertisement
    Android_Advertisement
    iOS_Advertisement
    Click Advertisement Save Button

InAppPurchase Control
    Click Monetization
    Click InAppPurchase
    InAppPurchase Settings
    InAppPurchase New Product
    InAppPurchase Edit Product
    InAppPurchase Delete Product