*** Settings ***
Resource   ../Resources/Utils.robot
Resource  ../Resources/EcommerceKeywords.robot
Library  SeleniumLibrary
Library  FakerLibrary  WITH NAME  faker
Test Setup  Utils.start app session
Test Teardown  Utils.close session

*** Test Cases ***

ECommerce Settings Control
    Click Ecommerce
    Ecommerce Settings
    Ecommerce Settings Delivery
    Ecommerce Settings Refund
    Sales Contract
    Notifications Emails
    Ecommerce Settings Payment Settings
    Ecommerce Settings Payment Cash on Delivery
    Ecommerce Settings Transfer
    Ecommerce Settings Iyzico
    Ecommerce Settings Cargo
    Ecommerce Settings Save Button

ECommerce Product Control
    Click Ecommerce
    Ecommerce Product
    Ecommerce Product Add Product
    Ecommerce Product Add Product Name
    Ecommerce Product Add Product Description
    Ecommerce Product Add Product Texts
    Ecommerce Product Add Product Image
    Ecommerce Product List Visible