#Base image
FROM ubuntu
LABEL version="latest"

WORKDIR /app
COPY TestCases TestCases
COPY PageObject PageObject
COPY Resources Resources

#update the image
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y tcl && apt-get install -y --no-install-recommends apt-utils

#install python
RUN apt install -y python3
RUN apt install -y python3-pip

#install robotframework, seleniumlibrary and fakerlibrary
RUN pip3 install robotframework
RUN pip3 install robotframework-seleniumlibrary
RUN pip3 install robotframework-faker

#The followig are needed for Chrome and Chromedriver installation
RUN apt-get install -y xvfb
RUN apt-get install -y zip
RUN apt-get install -y wget
RUN apt-get install ca-certificates
RUN apt-get install -y libnss3-dev libasound2 libxss1 libappindicator3-1 libindicator7 gconf-service libgconf-2-4 fonts-liberation libgbm1 xdg-utils
RUN wget https://dl.google.com./linux/direct/google-chrome-stable_current_amd64.deb
RUN dpkg -i google-chrome*.deb
RUN rm google-chrome*.deb
RUN wget -N https://chromedriver.storage.googleapis.com/86.0.4240.22/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip
RUN chmod +x chromedriver
RUN cp chromedriver /usr/local/bin
RUN rm chromedriver_linux64.zip

#Robot Specific
ENTRYPOINT ["robot"]